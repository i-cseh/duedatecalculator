<?php

namespace DueDateCalculator;

interface IDueDateCalculatorConfig
{
    /**
     * @return array
     */
    public function getWorkDays(): array;

    /**
     * @return int
     */
    public function getStartHour(): int;

    /**
     * @return int
     */
    public function getStartMinute(): int;

    /**
     * @return int
     */
    public function getEndtHour(): int;

    /**
     * @return int
     */
    public function getEndMinute(): int;

    /**
     * @return int
     */
    public function getWorkLength(): int;

    /**
     * @return int
     */
    public function getNrOfNonWorkingDays(): int;

    /**
     * @return string
     */
    public function getSubmitDateFormat(): string;

    /**
     * @return string
     */
    public function getReturnDateFormat(): string;
}
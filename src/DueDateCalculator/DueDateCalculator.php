<?php

namespace DueDateCalculator;

class DueDateCalculator
{
    /**
     * @var IDueDateCalculatorConfig
     */
    private $config;

    /**
     * DueDateCalculator constructor.
     * @param IDueDateCalculatorConfig $config
     */
    public function __construct(IDueDateCalculatorConfig $config)
    {
        $this->config = $config;
    }

    /**
     * Calculates the end date of the turnaround based on the given submit date.
     *
     * @param string $submitDate
     * @param int $turnaround
     * @return string
     * @throws \Exception
     */
    public function calculate(string $submitDate, int $turnaround): string
    {
        if (!$this->isValidTurnaround($turnaround)) {
            throw new \Exception('Invalid turnaround');
        }

        $dateTime = $this->getSubmitDateTime($submitDate);
        if (!$this->isWorkPeriod($dateTime)) {
            throw new \Exception('Invalida date: is not in work period');
        }

        // Calculate the number of days from the turnaround hours.
        $nrOfDays = intdiv($turnaround, $this->config->getWorkLength());
        // Store the remaining hours. Ex if turnaround 10m the remang hours will be 2.
        $remainingHours = $turnaround % $this->config->getWorkLength();


        $endDateTime = $this->getDayEndDateTime($dateTime);
        // Add remaining hours to the submitted date.
        $dateTime->modify("+$remainingHours hour");

        /* Handleif this additional hours is more then than work period:
         *  - get the diff between the end of the business day and the $dateTime
         *  - reset the $dateTime to the beginning of the business day
         *  - add the diff to the new $dateTime to ensure the proper hour calculation
         *  - incrase the $nrOdDays, because we gain an additional day.
        */
        if ($dateTime >= $endDateTime) {
            $diff = $endDateTime->diff($dateTime);
            $dateTime = $this->getDayStartDateTime($dateTime);
            $dateTime->add($diff);
            $nrOfDays++;
        }

        if ($nrOfDays) {
            // Calculate how many weekends are in our work period.
            $dayCounter = $dateTime->format('N') + 1 + $nrOfDays;
            $additionalDays = $nrOfDays + intdiv($dayCounter, 6) * $this->config->getNrOfNonWorkingDays();
            // Set up the end date.
            $dateTime->modify("+$additionalDays day");
        }

        return $dateTime->format($this->config->getReturnDateFormat());
    }

    /**
     * Converts submit date into \DateTime object.
     *
     * @param string $submitDate
     * @return bool|\DateTime
     * @throws \Exception
     */
    private function getSubmitDateTime(string $submitDate)
    {
        $dateTime = \DateTime::createFromFormat($this->config->getSubmitDateFormat(), $submitDate);
        if ($dateTime === false) {
            throw new \Exception('Invalid date format or value');
        }

        return $dateTime;
    }

    /**
     * Validates the turnaround.
     *
     * @param int $turnaround
     * @return bool
     */
    private function isValidTurnaround(int $turnaround): bool
    {
        return $turnaround > 0;
    }

    /**
     * Checks if the given date is in work period.
     *
     * @param \DateTime $dateTime
     * @return bool
     */
    private function isWorkPeriod(\DateTime $dateTime): bool
    {
        return $this->isWorkDay($dateTime) && $this->isWorkHour($dateTime);
    }

    /**
     * Checks if the given date is a workday.
     * @param \DateTime $dateTime
     * @return bool
     */
    private function isWorkDay(\DateTime $dateTime): bool
    {
        return in_array($dateTime->format('N'), $this->config->getWorkDays());

    }

    /**
     * Checks if the give \DateTime is between business hours.
     *
     * @param \DateTime $dateTime
     * @return bool
     */
    private function isWorkHour(\DateTime $dateTime): bool
    {
        return $this->getDayStartDateTime($dateTime) <= $dateTime && $dateTime < $this->getDayEndDateTime($dateTime);
    }

    /**
     * Returns the given \DateTime object with the starting business hour.
     *
     * @param $dateTime
     * @return \DateTime
     */
    private function getDayStartDateTime($dateTime): \DateTime
    {
        $startDateTime = clone $dateTime;
        return $startDateTime->setTime($this->config->getStartHour(), $this->config->getStartMinute());
    }

    /**
     * Returns the given \DateTime object with the ending business hour.
     * @param $dateTime
     * @return \DateTime
     */
    private function getDayEndDateTime($dateTime): \DateTime
    {
        $endDatetime = clone $dateTime;
        return $endDatetime->setTime($this->config->getEndtHour(), $this->config->getEndMinute());
    }

}
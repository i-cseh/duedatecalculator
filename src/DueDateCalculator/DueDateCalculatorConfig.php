<?php

namespace DueDateCalculator;

class DueDateCalculatorConfig implements IDueDateCalculatorConfig
{
    /**
     * @return array
     */
    public function getWorkDays(): array
    {
        return [
            1, //Monday
            2, //Tuesday
            3, //  Wednesday
            4, // Thursday
            5, // Friday
        ];
    }

    /**
     * @return int
     */
    public function getStartHour(): int
    {
        return 9;
    }

    /**
     * @return int
     */
    public function getStartMinute(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function getEndtHour(): int
    {
        return 17;
    }

    /**
     * @return int
     */
    public function getEndMinute(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function getWorkLength(): int
    {
        return 8;
    }

    /**
     * @return int
     */
    public function getNrOfNonWorkingDays(): int
    {
        return 7 - count($this->getWorkDays());
    }

    /**
     * @return string
     */
    public function getSubmitDateFormat(): string
    {
        return 'd/m/Y H:i';
    }

    /**
     * @return string
     */
    public function getReturnDateFormat(): string
    {
        return 'd/m/Y l H:i';
    }
}
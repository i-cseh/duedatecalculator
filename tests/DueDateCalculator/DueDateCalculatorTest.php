<?php

namespace DueDateCalculator;

use PHPUnit\Framework\TestCase;


class DueDateCalculatorTest extends TestCase
{
    /**
     * @var DueDateCalculator
     */
    private $dueDateCalculator;

    public function setUp()
    {
        $this->dueDateCalculator = new DueDateCalculator(new DueDateCalculatorConfig());
    }

    public function testCalculateInvalidDate()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Invalid date format or value');

        $date = 'unknown';
        $hours = 8;
        $this->dueDateCalculator->calculate($date, $hours);
    }

    public function testCalculateInvalidHours()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Invalid turnaround');

        $date = '01/01/2019 10:00';
        $hours = -1;
        $this->dueDateCalculator->calculate($date, $hours);
    }

    public function testInvalidBusinessHours()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Invalida date: is not in work period');

        $invalidHour = '30/08/2019 18:00';
        $hours = 1;
        $this->dueDateCalculator->calculate($invalidHour, $hours);
    }

    public function testInvalidBusinessDay()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Invalida date: is not in work period');

        $invalidDay = '31/08/2019 10:00';
        $hours = 1;
        $this->dueDateCalculator->calculate($invalidDay, $hours);

    }

    public function testCalculation()
    {
        $testData = $this->getTestCases();
        foreach ($testData as $data) {
            $result = $this->dueDateCalculator->calculate($data['date'], $data['hours']);
            $this->assertEquals($data['expectation'], $result);
        }
    }

    private function getTestCases()
    {
        return [
            [
                'date' => '30/08/2019 10:00',
                'hours' => 5,
                'expectation' => '30/08/2019 Friday 15:00'
            ],
            [
                'date' => '30/08/2019 10:00',
                'hours' => 8,
                'expectation' => '02/09/2019 Monday 10:00'
            ],
            [
                'date' => '30/08/2019 16:00',
                'hours' => 1,
                'expectation' => '02/09/2019 Monday 09:00'
            ],
            [
                'date' => '30/08/2019 16:00',
                'hours' => 49,
                'expectation' => '10/09/2019 Tuesday 09:00'
            ],
            [
                'date' => '30/08/2019 16:59',
                'hours' => 1,
                'expectation' => '02/09/2019 Monday 09:59'
            ],
            [
                'date' => '30/08/2019 15:59',
                'hours' => 9,
                'expectation' => '02/09/2019 Monday 16:59'
            ],
            [
                'date' => '02/09/2019 15:00',
                'hours' => 40,
                'expectation' => '09/09/2019 Monday 15:00'
            ],
        ];
    }
}

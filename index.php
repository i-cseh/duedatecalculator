<?php

use DueDateCalculator\DueDateCalculator;
use DueDateCalculator\DueDateCalculatorConfig;

require_once 'vendor/autoload.php';

function calculateDueDate($submitDate, $turnaround)
{
    $dueDateCalculator = new DueDateCalculator(new DueDateCalculatorConfig());
    try {
        $dueDate = $dueDateCalculator->calculate($submitDate, $turnaround);
    } catch (Exception $e) {
        // Handle exception.
        echo 'Something went wrong: ' . $e->getMessage();
    }
    return $dueDate;

}

echo calculateDueDate('30/08/2019 10:00', 5);